Merry-Go-Form Readme
====================

What is Merry-Go-Form?
----------------------

Merry-Go-Form is a simple javascript plugin that enables using carousel-based forms. Merry-Go-Form is just plain ol' javascript, so it has no dependencies.

Features:
- Basic form field validation
- Autosizing (sizing the form based on the largest interior slide)
- Customizable transitions and transition time
- Customizable submit

Quickstart
----------

A basic Merry-Go-Form with simple configuration (at the bottom of the <body> tag):

>    <script src="path/to/merry-go-form.min.js"></script>
>    <script type="text/javascript">
>    var mgf = new MerryGoForm();
>    mgf.init("form-id");
>    </script>

Note - Init can take either an ID or an actual DOM element, so you can pass `document.getElementById("some-selector")` (or some jquery equivalent).

How do I use Merry-Go-Form?
---------------------------

Merry-Go-Form is used by creating a form (like you normally would) in HTML markup, and separating the form elements into different "slides" by enclosing them in special divs (see minimum required section).
After separating your elements, ensure that the important elements all have the *name* attribute set on them for Merry-Go-Form to pick up.

After doing so, you can create the merry-go-form by using the constructor, and passing along the ID of the form along with any options.

>       var merryGoForm = new MerryGoForm('dogs-form',{
>      submit: function(f,data) { console.log("doing submit stuff!"); },
>      });


Minimum Required DOM
--------------------

The Merry-Go-Form expects at the very least to have form element with a div inside it that has the class "merry-go-slides". I.E.:

>    <form id="merry-go-form">
>    <div class="merry-go-slides"></div>
>    </form>

If there are no slides in your form, one will be automatically added for you.


What are Merry-Go-Form's Options?
---------------------------------

The Merry-Go-Form constructor accepts a few options, namely:

* *autoSize* (Boolean) - This determines whether the MerryGoForm will size itself to it's largest possible size (the largest interior slide's size) automatically
* *disableDefaultSlide* (Boolean) - Disable addition of default slide when `form > div.merry-go-slides` element is empty
* *transitionTime* (Integer) - Specity the amount of time (in milliseconds) to use for transitions
* *requiredFields* (Array<String>) - Fields (specified by the HTML attribute name on the element) that will be checked to be present and non-empty before submit occurs.
* *submit* (Function) - A submit function to use instead of the default HTMLForm submit function.
* *easingFunction* (Function) - Specify a custom easing function to use when doing transitions. For for example, a linear easing function looks like:

>    self.linearEasing = function(currentTime, startValue, changeInValue, duration, direction){
>        var multiplier = direction === 'left' ? -1 : 1;
>        return startValue + (multiplier * currentTime * changeInValue / duration);
>    };

NOTE - See demo.html for an example usage of each one of these options.


Build instructions
------------------

Requires [nodeJS](http://nodejs.org), [npm](http://npmjs.org), and [grunt](http://gruntjs.com).

To build MGF:
`npm install`
`grunt`

To test MGF:
`grunt test` - Unit tests
`grunt integration` - Integration tests

Contribute to Merry Go Form
---------------------------

Send us a pull request if you'd like to cintribute ot Merry Go Form!

License
-------

Merry Go Form is open source software under the [ISC license](http://opensource.org/licenses/ISC).

