/*global module:false*/
module.exports = function(grunt) {

    // Project config
    grunt.initConfig({
        // Metadata.
        pkg: grunt.file.readJSON('package.json'),
        banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
            '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
            '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
            '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
            ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
        
        // Copy 
        copy: {
            mgf: {
                files: {
                    "dist/merry-go-form.js": "src/merry-go-form.js"
                }
            }
        },

        // Uglify
        uglify: {
            mgf: {
                files: {
                    "dist/merry-go-form.min.js": ["src/merry-go-form.js"]
                }
            }
        },

        //minify css
        cssmin: {
            mgf: {
                files: [{
                    expand: true,
                    cwd: 'public/style/',
                    src: ['*.css', '!*.min.css'],
                    dest: 'dist/',
                    ext: '.min.css'
                }]
            }
        },

        // JSHint testing
        jshint: {
            src: ["src/**/*.js", "spec/**/*.js"],
            options: {
                // Disable functions within loop warning -- need them for closures
                loopfunc: true,
                scripturl: true
            }
        },

        // Task config goes here
        jasmine: {
            mgf: {
                src: ["src/merry-go-form.js"],
                options: {
                    specs: ["spec/**/*.js",
                            "!spec/acceptance/**/*.js",
                            "!spec/resources/**/*"]
                }
            }
        },

        // Node-powered jasmine tests
        jasmine_node: {
            options: {
                extensions: '.spec.js',
                matchall: true
            },
            acceptance: ['spec/acceptance/']
        },

        // Execute tasks upon changes
        watch: {
            scripts: {
                files: ['src/**/*.js'],
                tasks: ['jshint', 'jasmine'],
                options: {
                    spawn: false
                }
            }
        }

    });

    // Tasks loading (with grunt.loadNpmTasks) goes here
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-jasmine-node');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    // Tasks
    grunt.registerTask('test',['jasmine']);
    grunt.registerTask('build',['jshint', 'copy', 'uglify', 'cssmin']);
    grunt.registerTask('integration',['jasmine_node:acceptance']);
    grunt.registerTask('default',['test', 'build']);

    // Pre-deployment testing/tasks
    grunt.registerTask('pre-deploy',['test', 'build', 'integration']);

};
