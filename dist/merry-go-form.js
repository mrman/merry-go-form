/**
 * Merry Go Form
 *
 * @file Merry Go Form javascript source file
 * @copyright 2014
 */

// Fix for console.log (IE)
if (!window.console || !console) { console.log = function() {}; }

/**
 * Creates a MerryGoForm object
 *
 * @class
 */
function MerryGoForm() {
    var self = this;
    // Options (and defaults)
    self.disableDefaultSlide = false;
    self.autoSize = true;
    self.requiredFields = [];
    self.transitionType = 'slide';
    self.transitionTime = 500;
    self.transitionInterval = 2;
    self.easingType = 'linear';

    // Form elements/state
    self.form = null;
    self.submitBtn = null;
    self.resetBtn = null;
    self.forwardBtn = null;
    self.backBtn = null;
    self.progressIndicator = null;
    self.alerts = [];
    self.nextAlertID = 0;
    self.slidesContainer = null;
    self.currentSlideIndex = 0;
    self.transitioning = false;

    self.defaultSlideHtml = "<h2 class=\"title\">Thanks for using Merry-Go-Form!</h2>" +
        "<h3> Please add some slides!</h3>" +
        "<div class=\"description\">" +
        "<p>It looks like you don't have any slides in your merry go form, this slide was added by default.</p>" +
        "<p>If confused about how to create a valid slide, please check the documentation.</p>" +
        "</div>";

    /**
     * Utility functions for use in MGF codebase
     *
     * @protected
     * @namespace Util
     */
    self.Util = {
        isUndefined: function(obj) { return typeof obj === "undefined"; },
        isDefined: function(obj) { return !Util.isUndefined(obj); },
        isNull: function (obj) { return obj === null; },
        isString: function(obj) { return typeof obj === "string"; },
        isFunction: function(obj) {return Object.prototype.toString.call(obj) === '[object Function]'; },
        numericalSortFn: function(a,b) { return b - a; },

        /**
         * Safely (with support for IE) remove a DOM element
         * @param {object} elem - The element to be removed
         * @returns The element that was removed
         */
        removeDomElement: function(elem) {
            elem.parentNode.removeChild(elem);
            return elem;
        },

        /**
         * Utility function to create a new list with provided indices removed.
         * If callback is provided, it will be called on every element that was marked for removal.
         * @param {list} arr - The starting array
         * @param {list} indices - The list of indices to remove (integers)
         * @param {function} cb - The callback that will be executed for each removed item
         * @returns {list} array - The modified array
         */
        removeFromArray: function(arr,indices,cb) {
            // Check parameters
            if ((Util.isDefined(arr) && !(arr instanceof Array)) ||
                (Util.isDefined(indices) && !(indices instanceof Array)) ||
                (Util.isDefined(cb) && !(cb instanceof Function))) {
                self.throwError(Errors.INVALID_PARAMS_REMOVEFROMARRAY);
            }

            var result = [];
            // Create return array and call callbacks
            for(var i = 0; i < indices.length && i < arr.length; i++) {
                if (!(i in indices)) { result.push(arr[i]); }
                if (Util.isDefined(cb)) cb(arr[i]);
            }

            return result;
        }
    };
    var Util = self.Util;

    /**
     * Errors that can be thrown from merry go form objects
     * @enum {string}
     * @namespace Error
     */
    self.Errors = {
        INVALID_FORM_ID: "Invalid form id! Please provide a valid ID to the form you'd like to make into a merry-go-form",
        INVALID_INIT_ELEMENT: "Invalid initialization form ID/Element! Please provide either a valid form element ID or HTMLElement",
        MISSING_FORM_ELEMENT: "Failed to find element specified by form id!",
        MISSING_SLIDES_CONTAINER_ELEMENT: "Failed to find any container of slides with class \"merry-go-slides\". Please create a container for the slides",
        INVALID_PARAMS_REMOVEFROMARRAY: "Invalid Parameters to removeFromArray.",
        MISSING_REQUIRED_OPTIONS: "Form is missing required options!"
    };
    var Errors = self.Errors;

    /**
     * Utility functions for throwing errors, adds the error to be thrown to array to make testing easier
     * @param {string} msg - Error message
     */
    self.thrownErrors = [];
    self.throwError = function(msg) {
        var err = new Error(msg);
        self.thrownErrors.push(err);
        throw err;
    };

    /**
     * Initialize Form controls, primarily reset and submit
     * @returns {list} Returns a tuple of the submit and reset buttons
     */
    // throws error if it cannot find buttons with the right classes
    self.initFormControls = function() {
        var submitBtns = self.form.getElementsByClassName('merry-go-submit');
        var resetBtns = self.form.getElementsByClassName('merry-go-reset');

        // Setup submit button (if present)
        if (submitBtns.length > 0) {
            self.submitBtn = submitBtns[0];
            self.submitBtn.onclick = function(e) { self.form.submit(); };
        }

        // Setup reset button (if present)
        if (resetBtns.length > 0) {
            self.resetBtn = resetBtns[0];
            self.resetBtn.onclick = function(e) { self.form.reset(); };
        }

        return [self.submitBtn,self.resetBtn];
    };

    /**
     * Initialize navigation elements, primarily back and forward button overlays
     * @returns {list} Returns a tuple of back and forward buttons
     */
    self.initNavigation = function() {
        self.forwardBtn = document.getElementsByClassName('merry-go-forward')[0];
        self.backBtn = document.getElementsByClassName('merry-go-backward')[0];

        // Setup back button
        if (Util.isDefined(self.backBtn)) {
            self.backBtn.onclick = function(e){
                self.movePreviousSlide();
            };
        }

        // Setup forward button
        if (Util.isDefined(self.forwardBtn)) {
            self.forwardBtn.onclick = function(e){
                self.moveNextSlide();
            };
        }

        return [self.backBtn,self.forwardBtn];
    };

    /**
     * Initialize the Merry Go Form
     * @param {string|object} form - The form element (or ID of said element) for the Merry Go Form.
     * @param {object} options - Initialization options for the Merry Go Form
     *
     * @throws {Error} Will throw an error if no form is provided
     * @throws {Error} Will throw an error if the form element is invalid (or can't be found with document.getElementByID for string input)
     * @throws {Error} Will throw an error if the form provided is not a string or an HTMLElement
     * @throws {Error} Will throw an error if the form element does not contain an element named "merry-go-slides" (the slide container)
     * @returns {object} The Merry Go Form
     */
    self.init = function(form,options) {
        // Exit early if form is not provided
        if (Util.isUndefined(form)) { self.throwError(Errors.INVALID_FORM_ID); }
        options = options || {};

        // Ensure that form has been specified, get a pointer to the form
        if (Util.isString(form)) {
            self.form = document.getElementById(form);
        } else if (form instanceof HTMLElement) {
            self.form = form;
        } else {
            self.throwError(Errors.INVALID_INIT_ELEMENT);
        }

        // Ensure form is valid
        if (Util.isNull(self.form)) {
            self.throwError(Errors.MISSING_FORM_ELEMENT);
        }

        // Ensure the slide container is non-empty
        if (self.form.getElementsByClassName('merry-go-slides').length === 0) {
            self.throwError(Errors.MISSING_SLIDES_CONTAINER_ELEMENT);
        }
        self.slidesContainer = self.form.getElementsByClassName('merry-go-slides')[0];

        // Initialize form controls
        self.initFormControls();

        // Gather all the slides in the merry-go-form
        var slides = self.slidesContainer.getElementsByClassName('slide');

        // Create & add default slide if no slide was presented
        if ('disableDefaultSlide' in options) self.disableDefaultSlide = options.disableDefaultSlide;
        if (slides.length === 0 && !self.disableDefaultSlide) {
            var defaultSlideDiv = document.createElement("div");
            defaultSlideDiv.className = 'merry-go-start slide';
            defaultSlideDiv.innerHTML = self.defaultSlideHtml;
            self.slidesContainer.appendChild(defaultSlideDiv);
            slides = self.slidesContainer.getElementsByClassName('slide');
        }

        // Show the first slide
        var startSlideIndex = self.findStartSlideIndex();
        slides[startSlideIndex].style.display = 'inline-block';
        self.currentSlideIndex = startSlideIndex;

        if (!('autoSize' in options) || 
            ('autoSize' in options && options.autoSize)) {
            self.autoSizeSlideContainer();
        }

        if ('transitionTime' in options && !isNaN(options.transitionTime)) {
            self.transitionTime = options.transitionTime;
        }

        // Set easing function (whether provided as a string or a function)
        if ('easingFunction' in options &&
            Util.isDefined(options.easingFunction) &&
            !Util.isNull(options.easingFunction)) {

            if (Util.isString(options.easingFunction)) {
                self.easingType = options.easingFunction;
            } else if (Util.isFunction(options.easingFunction)) {
                self.easing.userDefined = options.easingFunction;
                self.easingType = 'userDefined';
            }
        }

        // Add a slide/progress bar indicator
        self.progressIndicator = self.form.getElementsByClassName('merry-go-position')[0];
        if(Util.isDefined(self.progressIndicator)) {
            self.initProgressIndicator();
        }

        // Initialize navigation (forward & back buttons)
        self.initNavigation();

        // Hide and reshow the buttons to fix issue in chrome/other browsers,
        // due to the lack of display of the slide, the buttons are overlaid on top of each other
        if (Util.isDefined(self.forwardBtn)) {
            self.forwardBtn.style.display = 'none';
            self.forwardBtn.style.display = 'inline-block';
        }

        // Un-hide the form (should be hidden by default)
        self.form.style.display = 'block';

        // Handle custom submit in the options
        if ('submit' in options) {
            // Override the form's submit method
            self.form.addEventListener('submit',function(e) {e.preventDefault();},false);
            this.form.submit = function() {options.submit(this.form,self.gatherData());};
        }

        // Set required fields if provided in options
        if ('requiredFields' in options && options.requiredFields instanceof Array) self.requiredFields = options.requiredFields;

        // Create submit wrapper function to perform validation
        var oldSubmitFn = this.form.submit;
        self.form.submit = function() {
            if (self.hasRequiredFields()) {
                oldSubmitFn();
            }
        };

        return self;
    };

    /**
     * Get all slides in the slide container (using getElementsByClassName)
     * @return {list} HTMLElements for all the slides in the slide container
     */
    self.getSlides = function() {
        return self.slidesContainer.getElementsByClassName('slide');
    };

    /**
     * Calculate and set the size of the slide container to the largest interior slide
     * Autosizes by:
     * 1) Rendering slides horizontally (slides must be displayed to get height, we just display them in an invisible place)
     * 2) Attaching function to document ready state which finds the tallest slide and sets it as minheight of container
     */
    self.autoSizeSlideContainer = function() {
        var slides = self.getSlides();

        self.renderSlidesHorizontally();
        document.onreadystatechange = function() {
            if (document.readyState === 'complete'){
                var heights = [];
                slides = self.getSlides();
                for (var i = 0; i < slides.length; i++) {
                    slides[i].style.display = 'inline-block';
                    heights.push(slides[i].offsetHeight);
                }
                var maxSlideHeight = Math.max.apply(null,heights);
                self.slidesContainer.style.minHeight = maxSlideHeight + "px";
            }
        };
    };

    /**
     * Render slides next to each other horizontally inside the slide container
     */
    self.renderSlidesHorizontally = function() {
        var slides = self.getSlides();
        var startingSlideIndex = self.findStartSlideIndex();
        for (var i = 0; i < slides.length; i++) {
            slides[i].style.top = 0;
            // Add 20 here to clear the space added by left and right buttons
            slides[i].style.left = (i*100 + 20) +"%";
            slides[i].style.float = 'right';
            // Only absolute position (and move) the not-shown slides, so the container doesn't empty malform
            if (i != startingSlideIndex) slides[i].style.position = 'absolute';
        }
    };

    /**
     * Find the index of the merry-go-start slide, if there is one
     */
    self.findStartSlideIndex = function() {
        var slides = self.slidesContainer.getElementsByClassName('slide');
        var defaultStartIndex = 0;
        for (var i = 0; i < slides.length; i++) {
            if (slides[i].className.search("merry-go-start") >= 0) {
                return i;
            }
        }
        return defaultStartIndex;
    };

    /**
     * Get the HTMLElement for a slide by it's index
     * @param {number} index - Index of the slide
     * @throws {Error} Will throw error if index is out of range
     * @returns {object} Returns slide dom element
     */
    self.getElementBySlideByIndex = function(index){
        var slides = self.slidesContainer.getElementsByClassName('slide');
        if (index >= 0 && index < slides.length){
            return slides[index];
        } else{
            self.throwError("Index out of range.",index);
        }
    };

    /**
     * Check that all required fields have been specified
     */
    self.hasRequiredFields = function() {
        var missingFields = self.getMissingRequiredFields();
        if (missingFields.length !== 0) {
            self.addAlert("error", "Please fill all of the following required fields: [" + missingFields.join(",") + "]");
            self.throwError(Errors.MISSING_REQUIRED_OPTIONS);
        }
        return missingFields.length === 0;
    };

    /**
     * Check if form has all required fields filled/defined
     */
    self.getMissingRequiredFields = function() {
        var missingRequiredFields = [];
        if (self.requiredFields.length === 0) return true;
        var data = self.gatherData();
        for (var i = 0; i < self.requiredFields.length; i++) {
            var field = self.requiredFields[i];

            // General check
            if (!(self.requiredFields[i] in data)) {
                missingRequiredFields.push(field);
                continue;
            }

            var val = data[field];
            if (Util.isUndefined(val) || Util.isNull(val)) {
                missingRequiredFields.push(field);
                continue;
            }

            // Special check for string values
            if (typeof val === "string" && val.trim() === "") {
                missingRequiredFields.push(field);
                continue;
            }

            // Special check for checkbox inputs (make sure there is at least one checked)
            if (val instanceof Object) {
                var oneTrue = false;
                var keys = Object.keys(val);
                for (var j = 0; j < keys.length; j++) oneTrue = oneTrue || val[keys[j]];
                if (!oneTrue) {
                    missingRequiredFields.push(field);
                    continue;
                }
            }
        }

        return missingRequiredFields;
    };

    /**
     * Gather data from form fields into one simple javascript object
     */
    self.gatherData = function() {
        var data = {};
        var childNodes = this.form.childNodes;

        // Process nodes in the form
        var elements = {selects: self.slidesContainer.getElementsByTagName('select'),
                        textareas: self.slidesContainer.getElementsByTagName('textarea'),
                        inputs: self.slidesContainer.getElementsByTagName('input')};

        // Handle selects
        for (var i = 0; i < elements.selects.length; i++) {
            var n = elements.selects[i];
            if (Util.isUndefined(n.name) || n.name === '') continue;
            data[n.name] = n.value;
        }

        // Handle textareas
        for (var j = 0; j < elements.textareas.length; j++) {
            var textAreaNode = elements.textareas[j];
            if (Util.isUndefined(textAreaNode.name) || textAreaNode.name === '') continue;
            data[textAreaNode.name] = textAreaNode.value;
        }

        // Inputs, require a little more thought
        for (var k = 0; k < elements.inputs.length; k++) {
            var inputNode = elements.inputs[k];
            var name = inputNode.name;
            var type = inputNode.type;
            if (Util.isUndefined(name) || name === '') continue;
            if (type === 'text') {
                data[name] = inputNode.value;
            } else if (type === 'number') {
                var num = (inputNode.value === "") ? 0 : parseInt(inputNode.value);
                if (isNaN(num)) {
                    self.addAlert("error","Invalid value given for field [" + name + "]");
                    self.throwError("Not a number!");
                }
                data[name] = num;
            } else if (type === 'checkbox') {
                if (!(name in data)) data[name] = [];
                if (inputNode.checked) data[name].push(inputNode.value);
            } else if (type === 'radio') {
                if (!(name in data)) data[name] = {};
                data[name][inputNode.value] = inputNode.checked;
            }
        }

        return data;
    };

    /////////////////
    // Transitions //
    /////////////////

    /**
     * Replace transition (simplest, hides the current slide and shows the next slide)
     *
     * @param {function} cb - Callback to be called on successful transition
     * @param {number} currentSlideIndex - Index of the current slide
     * @param {number} nextSlideIndex - Index of the next (destination) slide
     */
    self.transitionReplace = function(cb,currentSlideIndex, nextSlideIndex) {
        if (nextSlideIndex < self.slidesContainer.getElementsByClassName('slide').length &&
            nextSlideIndex >= 0){
            self.moveSlide(nextSlideIndex);
            if (Util.isDefined(cb)) cb(self.slides[nextSlideIndex]);
        }
    };

    /**
     * Slide transition function
     * @param {number} currentSlideIndex - Index of the current slide
     * @param {number} nextSlideIndex - Index of the next (destination) slide
     * @param {function} cb - Callback to be called on successful transition
     */
    self.transitionSlide = function(cb, currentSlideIndex, nextSlideIndex, easingType) {

        //TODO handle if the slide doesn't take 100% of the screen
        var currentTime = 0;
        var currentSlideDom = self.getElementBySlideByIndex(currentSlideIndex);
        var nextSlideDom = self.getElementBySlideByIndex(nextSlideIndex);
        if (currentSlideDom === null|| nextSlideDom === null){
            return null;
        } else {
            var nextSlideStartingLocation = 100;
            var curSlideStartingLocation = 0;
            var direction = "";
            if (currentSlideIndex < nextSlideIndex) {
                nextSlideDom.style.left = '100%';
                nextSlideStartingLocation = 100;
                direction = 'left';

            } else{
                nextSlideStartingLocation = -100;
                nextSlideDom.style.left = '-100%';
                direction = 'right';

            }
            nextSlideDom.style.display = 'inline-block';
            nextSlideDom.style.position = 'absolute';
            currentSlideDom.style.position = 'absolute';
            self.transitioning = true;
            var timer = setInterval(function(){

                var nextSlideLocation = self.easing[easingType || self.easingType](currentTime, nextSlideStartingLocation, 100, self.transitionTime, direction);
                var curSlideLocation = self.easing[easingType || self.easingType](currentTime, curSlideStartingLocation, 100, self.transitionTime, direction);
                nextSlideDom.style.left = '' + nextSlideLocation + '%';
                currentSlideDom.style.left = '' + curSlideLocation + '%';
                currentTime += self.transitionInterval;
                if ((direction === 'left' && nextSlideLocation <= curSlideStartingLocation + 0.1) || //adding 0.1 offset b/c of long time for exponentialOut
                    (direction == 'right' && nextSlideLocation >= curSlideStartingLocation - 0.1)){
                    clearInterval(timer);
                    nextSlideDom.style.left = '' + curSlideStartingLocation + '%'; //Putting the new slide to the original slide location in case of decimal offset
                    currentSlideDom.style.display = 'none';
                    self.transitioning = false;
                    if (Util.isDefined(cb)) cb(currentSlideDom);
                }
            }, self.transitionInterval);
            return self;
        }
    };

    /**
     * Easing functions are defined directly below. Functions (and function signature/structure) was borrowed from
     * They all follow a standardized format (much like this linear easing function) borrowed from an article at gizma.com (see below).
     *
     * @see {@link http://gizma.com/easing}
     * @param {number} currentTime - The current time
     * @param {number} startValue - The start value of the transition
     * @param {number} changeInValue - The delta (change in value) so far during the transition (from start value)
     * @param {number} duration - The overall duration of the transition
     * @param {string} direction - The direction of the transition
     */
    self.linearEasing = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        return startValue + (multiplier * currentTime * changeInValue / duration);
    };

    /**
     * Quadratic ease in function
     * @see {@link self.linearEasing} for more information
     */
    self.quadraticEasingIn = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        var time = currentTime / duration;
        return startValue + multiplier * time * time * changeInValue;
    };

    /**
     * Quadratic ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.quadraticEasingOut = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        var time = currentTime / duration;
        return startValue + multiplier * (-changeInValue * time * (time - 2));
    };

    /**
     * Quadratic ease in, ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.quadraticEasingInAndOut = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        var time = currentTime / duration / 2;
        if (time < 1) {
            return changeInValue / 2 * time * time * multiplier + startValue;
        } else {
            time--;
            return multiplier * (-changeInValue / 2) * (time * (time - 2) - 1) + startValue;
        }
    };

    /**
     * Cubic ease in function
     * @see {@link self.linearEasing} for more information
     */
    self.cubicEasingIn = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        var time = currentTime / duration;
        return startValue + multiplier * time * time * time * changeInValue;
    };

    /**
     * Cubic ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.cubicEasingOut = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        var time = currentTime / duration;
        time--;
        return startValue + multiplier * (changeInValue * (time * time * time + 1));
    };

    /**
     * Cubic ease in, ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.cubicEasingInAndOut = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        var time = currentTime / duration / 2;
        if (time < 1) {
            return changeInValue / 2 * time * time * time * multiplier + startValue;
        } else {
            time -= 2;
            return multiplier * (changeInValue / 2 * (time * time * time + 2)) + startValue;
        }
    };

    /**
     * Quartic ease in function
     * @see {@link self.linearEasing} for more information
     */
    self.quarticEasingIn = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        var time = currentTime / duration;
        return startValue + multiplier * time * time * time * time * changeInValue;
    };

    /**
     * Quartic ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.quarticEasingOut = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        var time = currentTime / duration;
        time--;
        return -changeInValue * (time*time*time*time - 1) * multiplier + startValue;
    };

    /**
     * Quartic ease in, ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.quarticEasingInAndOut = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        var time = currentTime / duration / 2;
        if (time < 1) {
            return changeInValue / 2 * time * time * time * time * multiplier + startValue;
        } else {
            time -= 2;
            return multiplier * (-changeInValue / 2 * (time * time * time * time - 2)) + startValue;
        }
    };

    /**
     * Quintic ease in function
     * @see {@link self.linearEasing} for more information
     */
    self.quinticEasingIn = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        var time = currentTime / duration;
        return startValue + multiplier * time * time * time * time * time * changeInValue;
    };

    /**
     * Quintic ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.quinticEasingOut = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        var time = currentTime / duration;
        time--;
        return startValue + multiplier * (changeInValue * (time * time * time * time * time + 1));
    };

    /**
     * Quintic ease in, ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.quinticEasingInAndOut = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        var time = currentTime / duration / 2;
        if (time < 1) {
            return changeInValue / 2 * time * time * time * time * time * multiplier + startValue;
        } else {
            time -= 2;
            return multiplier * (changeInValue / 2 * (time * time * time * time * time + 2)) + startValue;
        }
    };

    /**
     * Sinusoidal ease in function
     * @see {@link self.linearEasing} for more information
     */
    self.sinusoidalEasingIn = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        return (-changeInValue * Math.cos(currentTime / duration * (Math.PI / 2)) + changeInValue) * multiplier + startValue;
    };

    /**
     * Sinusoidal ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.sinusoidalEasingOut = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        return changeInValue * Math.sin(currentTime/duration* (Math.PI/2)) * multiplier + startValue;
    };

    /**
     * Sinusoidal ease in, ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.sinusoidalEasingInAndOut = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        return -changeInValue/2 * (Math.cos(Math.PI*currentTime/duration) - 1) * multiplier + startValue;
    };

    /**
     * Exponential ease in function
     * @see {@link self.linearEasing} for more information
     */
    self.exponentialEasingIn = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        return changeInValue * Math.pow( 2, 10 * (currentTime/duration - 1) ) * multiplier + startValue;
    };

    /**
     * Exponential ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.exponentialEasingOut = function(currentTime, startValue, changeInValue, duration, direction) {
        var multiplier = direction === 'left' ? -1 : 1;
        return changeInValue * ( -Math.pow(2, -10 * currentTime / duration) + 1 ) * multiplier + startValue;
    };

    /**
     * Exponential ease in, ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.exponentialEasingInAndOut = function(currentTime, startValue, changeInValue, duration, direction){
        var multiplier = direction === 'left' ? -1 : 1;
        var time = currentTime / duration /2;
        if (time < 1) {
            return changeInValue/2 * Math.pow( 2, 10 * (time - 1) ) * multiplier + startValue;
        }
        time--;
        return changeInValue/2 * ( -Math.pow( 2, -10 * time) + 2 ) * multiplier + startValue;
    };

    /**
     * Circular ease in function
     * @see {@link self.linearEasing} for more information
     */
    self.circularEasingIn = function(currentTime, startValue, changeInValue, duration, direction) {
        var multiplier = direction === 'left' ? -1 : 1;
        currentTime /= duration;
        return -changeInValue * (Math.sqrt(1 - currentTime * currentTime) - 1) * multiplier + startValue;
    };

    /**
     * Circular ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.circularEasingOut = function(currentTime, startValue, changeInValue, duration, direction) {
        var multiplier = direction === 'left' ? -1 : 1;
        currentTime /= duration;
        currentTime--;
        return changeInValue * Math.sqrt(1 - currentTime*currentTime) * multiplier + startValue;

    };

    /**
     * Circular ease in, ease out function
     * @see {@link self.linearEasing} for more information
     */
    self.circularEasingInAndOut = function(currentTime, startValue, changeInValue, duration, direction) {
        var multiplier = direction === 'left' ? -1 : 1;
        currentTime /= duration/2;
        if (currentTime < 1) return -changeInValue/2 * (Math.sqrt(1 - currentTime*currentTime) - 1) * multiplier + startValue;
        currentTime -= 2;
        return changeInValue/2 * (Math.sqrt(1 - currentTime*currentTime) + 1) * multiplier + startValue;

    };

    /**
     * Function Dispatch that contains all supported default easing functions
     */
    self.easing = {'linear': self.linearEasing, 'quadraticIn':self.quadraticEasingIn,
                    'quadraticOut':self.quadraticEasingOut, 'quadraticInAndOut':self.quadraticEasingInAndOut,
                    'cubicIn':self.cubicEasingIn, 'cubicOut':self.cubicEasingOut, 'cubicInAndOut':self.cubicEasingInAndOut,
                    'quarticIn':self.quarticEasingIn, 'quarticOut':self.quarticEasingOut,
                    'quarticInAndOut':self.quarticEasingInAndOut, 'quinticIn':self.quinticEasingIn,
                    'quinticOut':self.quinticEasingOut, 'quinticInAndOut':self.quinticEasingInAndOut,
                    'sinusoidalIn':self.sinusoidalEasingIn, 'sinusoidalOut':self.sinusoidalEasingOut,
                    'sinusoidalInAndOut':self.sinusoidalEasingInAndOut, 'exponentialIn':self.exponentialEasingIn,
                    'exponentialOut':self.exponentialEasingOut, 'exponentialInAndOut':self.exponentialEasingInAndOut,
                    'circularIn':self.circularEasingIn, 'circularOut':self.circularEasingOut,
                    'circularInAndOut':self.circularEasingInAndOut};

    /**
     * Move to next immediately adjacent slide (on the right)
     * @param {function} cb - Callback that will be called at the end of successful transition
     * @param {string} transitionType - Transition type (ex. "slide")
     */
    self.moveNextSlide = function(cb, transitionType) {
        var index = self.currentSlideIndex;
        var nextIndex = index + 1;
        if (nextIndex < self.slidesContainer.getElementsByClassName('slide').length &&
            nextIndex >= 0 && !self.transitioning) {

            // Dispatch next slide transition function, while wrapping the callback that was passed in
            // in a function that will update state for MGF after transition
            self.transitions[(transitionType || self.transitionType)](function(slideDom) {
                // Execute passedcallback like expected
                if (Util.isDefined(cb)) cb(slideDom);

                // Update MGF state (after successful transition)
                self.updateIndicators(nextIndex);
                self.currentSlideIndex = nextIndex;

            }, index, nextIndex);

        }
    };

    /**
     * Move to previous immediately adjacent slide (on the left)
     * @param {function} cb - Callback that will be called at the end of successful transition
     * @param {string} transitionType - Transition type (ex. "slide")
     */
    self.movePreviousSlide = function(cb, transitionType) {
        var index = self.currentSlideIndex;
        var nextIndex = index - 1;
        if (nextIndex < self.slidesContainer.getElementsByClassName('slide').length &&
            nextIndex >= 0 && !self.transitioning) {

            // Dispatch next slide transition function, while wrapping the callback that was passed in
            // in a function that will update state for MGF after transition
            self.transitions[(transitionType || self.transitionType)](function(slideDom) {
                // Execute passedcallback like expected
                if (Util.isDefined(cb)) cb(slideDom);

                // Update MGF state (after successful transition)
                self.updateIndicators(nextIndex);
                self.currentSlideIndex = nextIndex;

            }, index, nextIndex);

        }
    };

    /**
     * Move from one slide to another by destination slide index
     * @param {number} destinationSlideIndex - Index of the destination slide
     */
    self.moveSlide = function(destinationSlideIndex){
        self.hideAllSlides();
        var slide = self.slidesContainer.getElementsByClassName('slide')[destinationSlideIndex];
        slide.style.display = 'inline-block';
        if ('left' in slide.style && slide.style.left !== "0%") slide.style.left = "0%";

        // Update MGF state
        self.updateIndicators(destinationSlideIndex);
        self.currentSlideIndex = destinationSlideIndex;
    };

    /**
     * Move from slide x to slide y sequentially (transitioning through every slide in between).
     * This function is recursive.
     * @param {number} destinationSlideIndex - Index of the destination slide
     */
    self.moveSlideSequential = function(destinationSlideIndex){
        // Slide and recursive call through callback if different
        if (self.currentSlideIndex < destinationSlideIndex) {
            self.moveNextSlide(function(elem) {
                self.moveSlideSequential(destinationSlideIndex);
            });
        } else if (self.currentSlideIndex > destinationSlideIndex) {
            self.movePreviousSlide(function(elem) {
                self.moveSlideSequential(destinationSlideIndex);
            });
        }
    };

    /**
     * Move to a slide by ID
     * @param {string} transitionType - transition type to use
     * @param {number} destinationSlideID - The ID of the destination slide
     */
    self.moveSlideById = function(transitionType, destinationSlideId){
        transitionType = transitionType || self.transitionType;
        var slides = self.slidesContainer.getElementsByClassName('slide');
        for (var i = 0; i < slides.length; i++){
            if (slides[i].id === destinationSlideId){
                self.moveSlide(transitionType, i);
            }
        }
    };

    /**
     * Hide all slides
     */
    self.hideAllSlides = function() {
        var slides = self.slidesContainer.getElementsByClassName('slide');
        for (var i = 0; i < slides.length; i++){
            slides[i].style.display = 'none';
        }
    };

    /**
     * Function Dispatch that contains all supported slide transitions
     */
    self.transitions = {'replace': self.transitionReplace,
                        'slide': self.transitionSlide};


    /**
     * Initialize the progress indicator
     */
    self.initProgressIndicator = function(){
        var slides = self.slidesContainer.getElementsByClassName('slide');
        for(var i = 1; i <= slides.length; i++){
            self.progressIndicator.innerHTML += '<div class="indicator inactive"></div>';
        }
        var indicators = self.progressIndicator.getElementsByClassName('indicator');
        indicators[self.findStartSlideIndex()].className = 'indicator active';
        // hookup click events to indicators
        for(var j = 0; j < indicators.length; j++) {
            var currentSlide = self.getSlides()[j];
            var slideH2 = currentSlide.getElementsByTagName('h2')[0];
            indicators[j].onclick = (function (index) {
                return function (e) {
                    self.updateIndicators(index);
                    self.moveSlideSequential(index);
                };
            })(j);
            if(Util.isDefined(slideH2)){
                var toolTipDomElem = document.createElement('div');
                toolTipDomElem.appendChild(document.createTextNode( slideH2.innerHTML));
                toolTipDomElem.style.display = 'none';
                toolTipDomElem.className = 'merry-go-tip';
                indicators[j].appendChild(toolTipDomElem);
                indicators[j].onmouseover = (function (indicator) {
                    return function (e) {
                        var toolTip = indicator.getElementsByClassName('merry-go-tip')[0];
                        toolTip.style.display = 'inline-block';
                        var toolBottom = indicator.offsetTop - toolTip.offsetHeight - 10;
                        var widthOffset = Math.round((toolTip.offsetWidth - indicator.offsetWidth) / 2);
                        var toolLeft = indicator.offsetLeft - widthOffset;
                        toolTip.style.top = toolBottom + 'px';
                        toolTip.style.left = toolLeft + 'px';
                    };
                })(indicators[j]);
                indicators[j].onmouseout = (function (indicator) {
                    return function (e) {
                        indicator.getElementsByClassName('merry-go-tip')[0].style.display = 'none';
                    };
                })(indicators[j]);
            }
        }
    };

    /**
     * Update indicators, activate current slide's indicator
     */
    self.updateIndicators = function(activeIndex){
        var indicators = self.progressIndicator.getElementsByClassName('indicator');
        for(var i = 0; i < indicators.length; i++){
            indicators[i].className = 'indicator inactive';
        }
        indicators[activeIndex].className = 'indicator active';
    };

    /////////////
    // Alerts  //
    /////////////

    // Generate a new AlertID
    /**
     * Generate a sequential Alert ID
     * @return {number} A new Alert ID
     */
    self.generateAlertID = function() { return self.nextAlertID++; };

    /**
     * Add an alert
     * @param {string} type - The type of the alert
     * @param {string} mesasge - Text (including HTML) of the alert message
     * @return {object} The created alert
     */
    self.addAlert = function(type,msg) {
        // Update self alerts array
        var alert = {id: self.generateAlertID(),
                     type: type || "info",
                     msg: msg || "Invalid Alert",
                     domElements: [] };

        // Remove the last alert of this type and push in a new element
        self.removeAlertsByType(alert.type);
        self.alerts.push(alert);

        // Update DOM (alert containers specified) & alert domElements property, if self.form is defined
        if (self.form) {
            // Create dom element for removing the alert
            var alertRemove = document.createElement("a");
            alertRemove.onclick = function() { self.deleteAlertById(alert.id); };
            alertRemove.className = "remove";
            alertRemove.href = "javascript:void(0)";
            alertRemove.innerHTML = "&times;";

            // Possibly multiple alert containers
            var alertContainers = self.form.getElementsByClassName("merry-go-alerts");

            for (i = 0; i < alertContainers.length; i++) {
                var elem  = document.createElement("div");
                elem.className = "merry-go-alert " + alert.type;
                elem.id = "merry-go-alert-" + alert.id;
                elem.innerHTML = msg;
                // Add a close button to the inner html
                elem.appendChild(alertRemove);
                alertContainers[i].appendChild(elem);
                alert.domElements.push(elem);
            }
        }

        return alert;
    };

    /**
     * Remove an alert (and it's associated DOM) by type.
     * @param {string} type="info" - The type of the alerts you want to delete
     * @returns {list} alerts - this.alerts (modified)
     */
    self.removeAlertsByType = function(type) {
        type = type || "info";
        var alertIndicesToRemove = [];

        // Populate list of indices we want to remove
        for (var i = 0; i < this.alerts.length; i++) {
            if (type === this.alerts[i].type) {
                alertIndicesToRemove.push(i); // Save index for deletion later
            }
        }

        // Remove the alerts (and update DOM)
        this.alerts = Util.removeFromArray(this.alerts,
                                           alertIndicesToRemove,
                                           function(elem) {
                                               for (var i = 0; i < elem.domElements.length; i++) {
                                                   Util.removeDomElement(elem.domElements[i]);
                                               }
                                           });
        return this.alerts;
    };

    /**
     * Delete an alert (and it's associated DOM) by ID
     * @param {number} id - The ID of the alert to delete
     * @returns {list} alerts - this.alerts (modified)
     */
    self.deleteAlertById = function(id) {
        var indicesToRemove = [];
        // Gather matching alerts to delete
        for (var i = 0; i < self.alerts.length; i++) {
            if (self.alerts[i].id == id) indicesToRemove.push(i);
        }

        this.alerts = Util.removeFromArray(this.alerts,
                                           indicesToRemove,
                                           function(elem) {
                                               for (var i = 0; i < elem.domElements.length; i++) {
                                                   Util.removeDomElement(elem.domElements[i]);
                                               }
                                           });
        return this.alerts;
    };

}
