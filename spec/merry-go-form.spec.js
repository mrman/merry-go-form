/* MerryGoForm Jasmine Test Spec */

/**
 * Test Fixtures
 *
 */
var fixtures = {
    forms: {
        // Valid forms
        valid: {
            bare: {
                generate: function() {
                    var formElem = document.createElement("form");
                    var slidesContainer = document.createElement("div");
                    slidesContainer.className = "merry-go-slides";
                    formElem.appendChild(slidesContainer);
                    return formElem;
                }
            }
        },

        // Invalid forms
        invalid: {
                missingSlideContainer: {
                    generate: function() {
                        var formElem = document.createElement("form");
                        return formElem;
                    }
                }
            }
        }
};

///////////////////////
// Test Construction //
///////////////////////

describe("Merry-Go-Form constructor", function() {

    beforeEach(function() {
        // Spy on what's printed to the console
        spyOn(console,'log').and.callThrough();
    });

    it("should error upon use of the constructor with no arguments", function() {
        var mgf = new MerryGoForm();

        expect(mgf).not.toBeUndefined();
        expect(mgf).not.toBeNull();
    });

    it("should return expected fields when created with no options (and unitialized)", function() {
        var mgf = new MerryGoForm();

        expect(mgf.form).toBeNull();
        expect(mgf.submitBtn).toBeNull();
        expect(mgf.resetBtn).toBeNull();
        expect(mgf.forwardBtn).toBeNull();
        expect(mgf.backBtn).toBeNull();
        expect(mgf.progressIndicator).toBeNull();
        expect(mgf.requiredFields).toBeDefined();
        expect(mgf.requiredFields.length).toEqual(0);
        expect(mgf.alerts).toBeDefined();
        expect(mgf.alerts.length).toEqual(0);
        expect(mgf.nextAlertID).toEqual(0);
        expect(mgf.transitionType).toEqual("slide");
        expect(mgf.slidesContainer).toBeNull();
        expect(mgf.defaultSlideHtml).not.toBeNull();
    });

});

describe("Merry-Go-Form init", function() {
    it("should throw INVALID_FORM_ID error when a valid ID is not specified", function() {
        var mgf = new MerryGoForm();

        expect(function() {
            mgf.init();
        }).toThrow(new Error(mgf.Errors.INVALID_FORM_ID));
    });

    it("should throw the MISSING_FORM_ELEMENT error when a valid ID is not specified", function() {
        var mgf = new MerryGoForm();

        expect(function() {
            mgf.init("bad-id");
        }).toThrow(new Error(mgf.Errors.MISSING_FORM_ELEMENT));
    });

    it("should throw the INVALID_INIT_ELEMENT error when a non-string/non-HTMLElement is given", function() {
        var mgf = new MerryGoForm();

        expect(function() {
            mgf.init({});
        }).toThrow(new Error(mgf.Errors.INVALID_INIT_ELEMENT));
    });

    it("should work with a DOM element", function() {
        var mgf = new MerryGoForm();
        var formElem = fixtures.forms.valid.bare.generate();

        expect(function() {
            mgf.init(formElem);
        }).not.toThrow();
    });

    it("should throw an error if DOM element is missing slides container", function() {
        var mgf = new MerryGoForm();
        var formElem = fixtures.forms.invalid.missingSlideContainer.generate();

        expect(function() {
            mgf.init(formElem);
        }).toThrow(new Error(mgf.Errors.MISSING_SLIDES_CONTAINER_ELEMENT));
    });

});


/////////////////
// Test Alerts //
/////////////////

describe("Merry-Go-Form alerts", function() {
    var mgf;

    beforeEach(function() {
        mgf = new MerryGoForm();
    });

    it("should generate continuous AlertIDs, starting from zero",function() {
        var first = mgf.generateAlertID();
        var second = mgf.generateAlertID();

        expect(first).toEqual(0);
        expect(second).toBeGreaterThan(0);
        expect(first).not.toEqual(second);
        expect(first).toBeLessThan(second);
    });

    it("Should add a valid info alert",function() {
        var alert = mgf.addAlert("info",
                                 "This is a valid alert");

        expect(alert.id).toBeDefined();
        expect(alert.id).toBe(0);
        expect(mgf.alerts.length).toEqual(1);
        expect(mgf.alerts[0]).toEqual({id:0,
                                       type: "info",
                                       msg:"This is a valid alert",
                                       domElements: []});
    });

});
