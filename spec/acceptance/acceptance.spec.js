/**
 * Acceptance tests for Merry Go Form, using NightmareJS + PhantomJS
 *
 * These test are based on the demo.html page, which includes a basic form, with 3 slides.
 *
 * Known UI quirks:
 * - When no slide has occurred, the first slide will have a left value of 20%
 */

var path = require('path'),
    Nightmare = require('nightmare'),
    _ = require('lodash');

// Get the path to demo.html
var pathToDemoHTML = 'file:///' + path.resolve(__filename, "../../../demo.html");
var nightmare;

// Nightmare plugins

/**
 * NightmareJS plugin for moving MGF forward/backward one slide.
 * Click button.merry-go-forward or button.merry-go-backward button once and wait until mgf changes currentSlideIndex
 *
 * @param {string} direction - The direction to move slide in
 * @param {number} currentSlideIndex - The current slide index
 */
moveOneSlide = function(direction, currentSlideIndex) {
    // Decide where to move next
    var nextSlideIndex = currentSlideIndex;
    var buttonSelector = "";
    if (direction === "forward") {
        nextSlideIndex++;
        buttonSelector = ".merry-go-forward";
    } else if (direction === "backward") {
        nextSlideIndex--;
        buttonSelector = ".merry-go-backward";
    } else {
        throw Error("Invalid direction provided, needs to be either 'forward' or 'backward'");
    }

    return function(nightmare) {
        nightmare
            .click(buttonSelector)
            .wait(function() { return merryGoForm.currentSlideIndex; }, nextSlideIndex);
    };
};

describe("Merry-Go-Form forward navigation button", function() {
    beforeEach(function() {
        nightmare = new Nightmare()
            .goto(pathToDemoHTML);
    });

    it("should exist", function(done) {
        nightmare
            .evaluate(function(page) {
                return document.getElementsByClassName("merry-go-forward").length > 0;
            }, function(exists) {
                expect(exists).toBe(true);
                done();
            })
            .run();
    });

    // Tests for forwards navigation
    it("should move forward one slide when pressed", function(done) {
        nightmare
            .use(moveOneSlide("forward", 0))
            .evaluate(function(page) {
                // Expect MGF to reflect move from slide @ index 0 to 1
                return {
                    currentSlideIndex: merryGoForm.currentSlideIndex,
                    secondSlideLeftProperty: Math.floor(parseInt(document.getElementsByClassName("slide")[1].style.left))
                };
            }, function(results) {
                expect(results.currentSlideIndex).toBe(1);
                expect(results.secondSlideLeftProperty >= 0).toBeTruthy();
                expect(results.secondSlideLeftProperty).toBeLessThan(100);
                expect(results.secondSlideLeftProperty).not.toBeCloseTo(100, 20); // Shouldn't be near 100
                done();
            })
            .run();
    });
});

describe("Merry-Go-Form backward navigation button", function() {
    var originalTimeout;

    beforeEach(function() {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

        nightmare = new Nightmare()
            .goto(pathToDemoHTML);
    });

    it("should exist", function(done) {
        nightmare
            .evaluate(function(page) {
                return document.getElementsByClassName("merry-go-backward").length > 0;
            }, function(exists) {
                expect(exists).toBe(true);
                done();
            })
            .run();
    });

    it("should do nothing when pressed on the first slide", function(done) {
        nightmare
        .use(moveOneSlide("backward", 0))
        .evaluate(function(page) {
            return {
                currentSlideIndex: merryGoForm.currentSlideIndex,
                firstSlideLeftProperty: Math.floor(parseInt(document.getElementsByClassName("slide")[0].style.left)),
            };
        }, function(results) {
            expect(results.currentSlideIndex).toBe(0);
            expect(results.firstSlideLeftProperty >= 0).toBeTruthy();
            expect(results.firstSlideLeftProperty).toBeLessThan(100);
            done();
        })
        .run();
    });

    // Tests for forward/backwards navigation
    it("should move backward one slide after moving forward one slide", function(done) {
        nightmare
            .use(moveOneSlide("forward", 0))
            .use(moveOneSlide("backward", 1))
            .evaluate(function(page) {
                // Expect MGF to reflect move from slide @ index 0 to 1
                return {
                    currentSlideIndex: merryGoForm.currentSlideIndex,
                    firstSlideLeftProperty: Math.floor(parseInt(document.getElementsByClassName("slide")[0].style.left)),
                };
            }, function(results) {
                expect(results.currentSlideIndex).toBe(0);
                expect(results.firstSlideLeftProperty >= 0).toBeTruthy();
                expect(results.firstSlideLeftProperty).toBeLessThan(100);
                done();
            })
            .run();
    });

    afterEach(function() {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

});

describe("Merry-Go-Form slide indicators", function() {
    beforeEach(function() {
        nightmare = new Nightmare()
            .goto(pathToDemoHTML);
    });

    it("should have created an indicator for every slide", function(done) {
        nightmare
        .evaluate(function(page) {
            return {
                asManyIndicatorsAsSlides: document.getElementsByClassName("slide").length === document.getElementsByClassName("indicator").length,
                asManyTipsAsIndicators: document.getElementsByClassName("indicator").length === document.getElementsByClassName("merry-go-tip").length,
            };
        }, function(results) {
            expect(results.asManyIndicatorsAsSlides).toBe(true);
            expect(results.asManyTipsAsIndicators).toBe(true);
            done();
        })
        .run();
    });
});

describe("Merry-Go-Form form validation", function() {
    beforeEach(function() {
        nightmare = new Nightmare()
            .goto(pathToDemoHTML);
    });


    it("should throw a javascript error when form is missing required options", function(done) {
        nightmare
            .click(".merry-go-submit")
            .evaluate(function(page) {
                // Merry go form should have thrown an error and recorded it in it's thrownErrors array
                var expectedErrorMessage = merryGoForm.Errors.MISSING_REQUIRED_OPTIONS;
                for (var i = 0; i < merryGoForm.thrownErrors.length; i++) {
                    if (merryGoForm.thrownErrors[i].message === expectedErrorMessage) return true;
                }
                return false;
            }, function(threwRightError) {
                expect(threwRightError).toBe(true);
                done();
            })
            .run();
    });

    it("should show an alert form is submitted missing required options", function(done) {
        nightmare
        .click(".merry-go-submit")
        .wait("#merry-go-alert-0")
        .evaluate(function(page) {
            var elem = document.getElementById("merry-go-alert-0");
            var contains = function(str, query) { return str.indexOf(query) >= 0; };
            return {
                mgfAlertClassIsPresent: contains(elem.className, "merry-go-alert"),
                alertClassIsPresent: contains(elem.className, "alert"),
                expectedFieldsMissing: contains(elem.innerHTML, "name") && contains(elem.innerHTML, "sex")
            };
        }, function(results) {
            expect(results.mgfAlertClassIsPresent).toBe(true);
            expect(results.alertClassIsPresent).toBe(true);
            expect(results.expectedFieldsMissing).toBe(true);
            done();
        })
        .run();
    });

});

describe("Merry-Go-Form submit buttons", function() {
    beforeEach(function() {
        nightmare = new Nightmare()
            .goto(pathToDemoHTML);
    });

    it("should exist", function(done) {
        nightmare
        .wait(".merry-go-submit")
        .evaluate(function(page) {
            return document.getElementsByClassName("merry-go-submit");
        }, function(dom) {
            expect(dom.length).toBeGreaterThan(0);
            done();
        })
        .run();
    });

});

describe("Merry-Go-Form reset buttons", function() {
    beforeEach(function() {
        nightmare = new Nightmare()
            .goto(pathToDemoHTML);
    });

    it("should exist", function(done) {
        nightmare
        .wait(".merry-go-reset")
        .evaluate(function(page) {
            return document.getElementsByClassName("merry-go-reset");
        }, function(dom) {
            expect(dom.length).toBeGreaterThan(0);
            done();
        })
        .run();
    });

});
